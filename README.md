# Notes
For feature me

---

* Link: [https://semver.org/lang/ru/](https://semver.org/lang/ru/)
* Tags: [семвер] [семантическое версионирование] [минорная версия] [мажорная версия] [патч]
* Description: Схема версионирования **X.Y.Z** 

---

* Link: [https://github.com/PHPMailer/PHPMailer](https://github.com/PHPMailer/PHPMailer)
* Tags: [почта] [php] [library] [библиотека] [пхп]
* Description: PHPMailer - A full-featured email creation and transfer class for PHP

---

* Link: [http://rus-linux.net/MyLDP/consol/find-files-with-the-command-locate.html](http://rus-linux.net/MyLDP/consol/find-files-with-the-command-locate.html)
* Tags: [locate] [linux] [быстрый поиск] [find]
* Description: locate - быстрый поиск файла по файловой системе в linux (скорость достигается за счет того, что locate использует свою базу для хранения имен файлов)

---

* Link: [http://packetlife.net/library/cheat-sheets/](http://packetlife.net/library/cheat-sheets/)
* Tags: [cheat sheets] [заметки] [сеть] [network] [шпаргалки] [ports]
* Description: Шпаргалки посвещенный сети. 

---

* Link: [https://www.iana.org/assignments/port-numbers](https://www.iana.org/assignments/port-numbers)
* Tags: [ports] [порты] [сеть] [network] [list]
* Description: Список UDP/TCP портов популярных сервисов и приложений

---


template..

* Link: []()
* Tags: [] [] [] [] []
* Description: 
